
import sys

# listRegularFiles
import os
from os.path import isfile, join

# time now
import time

# hashedFloat
import random

# clustering
from sklearn import cluster


def retardedFloat(string):
    num = 0
    i = 1.0
    for c in string:
        num += i * ord(c)
        i /= 10

    return num


def hashedFloat(string):
    random.seed(string)
    return random.random()


def listRegularFiles(path):
    return [(path, f) for f in os.listdir(path) if isfile(join(path, f))]  


def extractMetadata(files, unixtime):
    data = []

    for t in files:
        path = t[0] + t[1]
        filename = t[1] 

        st = os.stat(path)

        name, extension = os.path.splitext(path) 

        d = (
            retardedFloat(filename),
            hashedFloat(extension), 
            float(st[6]), # 2 - size
            float(unixtime - st[7]), # 3 - time since access
            float(unixtime - st[8])  # 4 - time since modification
        )

        data.append(d)

    return data


# MAIN
path = "/home/salamon/down/"
unixtime = int(time.time())

files = listRegularFiles(path)

data = extractMetadata(files, unixtime)

# example of data
print(data[0])

# for d in data:
#     print(d)

# MACHINE LEARNING
clusters = 5

cl = cluster.KMeans(n_clusters=clusters, random_state=0).fit(data)

print("\n### LABELS ###")
for label in range(0, cl.n_clusters):

    print("\n### LABEL: {0} ###".format(label))

    i = 0
    while True:
        if label == cl.labels_[i]:
            print("{0}".format(files[i][1]))
        i += 1
        if i == len(files):
            break


print("\n### CLUSTER_CENTERS ###")
print(cl.cluster_centers_)

